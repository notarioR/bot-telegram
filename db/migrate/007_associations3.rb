class Associations3 < ActiveRecord::Migration
	def change
		remove_columns :plans, :user_id
		change_table :plans do |t|
	    t.belongs_to :user
	  end
	end
end
