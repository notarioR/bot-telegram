class CreatesUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
			t.integer :telegram_id      
      t.timestamps null: false
    end
  end
end