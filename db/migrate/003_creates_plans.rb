class CreatesPlans < ActiveRecord::Migration
  def change
    create_table :plans do |t|
      t.string :title
			t.text :content
			t.datetime :dateplan      
      t.timestamps null: false
    end
  end
end