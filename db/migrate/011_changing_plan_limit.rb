class ChangingPlanLimit < ActiveRecord::Migration
	def change
		change_column :users, :plan_limit, :integer, default: 0
  end
end