class Associations2 < ActiveRecord::Migration
	def change
		remove_columns :plans, :user_id
		change_table :plans do |t|
	    t.references :user
	  end
	end
end
