class WaitTime < ActiveRecord::Migration
  def change
    add_column :users, :wait_plan, :boolean, :default => false, :null => false
    add_column :users, :wait_time, :datetime
  end
end