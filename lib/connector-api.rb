class ConnectorApi

  def initialize(token)
    @token = token
  end

  def connection_api
    Telegram::Bot::Client.run(@token) do |bot|
      bot.listen do |message|
        options = {bot: bot, message: message, chat: message.chat} 
          MessageRespond.new(options).respond if message.text != nil
      end
    end
  rescue Telegram::Bot::Exceptions::ResponseError => e
  # If telegram gave a 502, it's safe to restart
    if (e.error_code.to_s == "502")
      puts "Got error 502 from Telegram, restarting in 10 seconds"
      sleep 10
      main
    else
      puts e
      exit 255
    end
  end
  
end