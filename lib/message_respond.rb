require_relative 'botan'

class MessageRespond
  attr_reader :message
  attr_reader :bot
  attr_reader :chat
  
  def initialize(options)
    @bot     = options[:bot]
    @message = options[:message]
    @chat    = options[:chat]
    conex    = DatabaseConector.new.establish_connection
  end

  def respond
    if include_the_following_text?('/start')
      hi(message)
      message_json = { text: message.text }

      puts Botan.track("btus:TXv6MM_EUDtWg3qCNb1e2WWEmG2", message.from.id, message_json, 'Search')
    end
    if include_the_following_text?('/emojize')
      em = Emojize.emojize(message.text.split(" ",2)[1])
      send_message(em)
    end
    if include_the_following_text?('/addplan')
      instrucction_to_add_plan(message)
    end
    if include_the_following_text?('/plans')
      get_plans
    end
    if include_the_following_text?('/delete') && search_plan_by_title
      delete_plan
    end
    if include_plan?
      get_plan_by_user
    end
    if include_the_following_text?('/join') && search_plan_by_title
      join_plan(user_telegram_id, message.text.split(" ", 2)[1]+"\n")
    end
    save_plan(message)
  end

  private

  def hi(message)
    users_from_chat = Plan.where(chat_id: message.chat.id)
    users_name = []
    users_from_chat.each do |u|
      User.where(id: u.user_id).each do |n|
        #binding.pry
        users_name << "/plan "+n.name if !users_name.include?("/plan "+n.name)
      end
    end
    kb = ReplyMarkupFormatter.new(users_name).get_markup
    bot.api.sendMessage(chat_id: message.chat.id, text: "Ver plan del usuario:", reply_markup: kb)
  end

  def instrucction_to_add_plan(message)
    if User.find_by(telegram_id: message.from.id) == nil
      user = User.create(name: message.from.first_name, telegram_id: message.from.id, wait_plan: true)
    else
      puts "This user already exists in our database."
      user = User.find_by(telegram_id: message.from.id)
      user.update(wait_plan: true, wait_time: Time.now )
    end
      send_message("OK. Enviame una lista de comandos para añadir tu plan. 
      Por favor usa este formato:
      <b>title -</b> Título
      <b>content -</b> Descripción del plan
      <b>date -</b> dd-mm-yyyy
      ")
  end

  def get_plans
    plans_chat = Plan.where(chat_id: message.chat.id)
    plans_chat.each do |p|
      send_message("<b>#{p.title}</b>#{p.content}#{p.dateplan}\n<b>Creado por:</b> #{p.user.name}")
    end
  end

  def delete_plan
    message_split   = message.text.split(" ", 2)
    plan_to_delete  = Plan.find_by( title: message_split[1]+"\n", 
                                    chat_id: message.chat.id, 
                                    user_id: User.find_by(telegram_id: message.from.id).id)
    if plan_to_delete 
      plan_to_delete.destroy
      send_message("Plan eliminado")
    else
      user_name = User.find_by(id: User.find_by(id: search_plan_by_title.user_id)).name
      send_message("Error, este plan pertenece a #{user_name}")
    end
  end

  def get_plan_by_user
      plans_chat = Plan.where(chat_id: message.chat.id, 
                              user_id: User.find_by(name: message.text.split(" ")[1]).id)
      plans_chat.each do |p|
        if p.users_join
          text = ""
          p.users_join.each_with_index do |i, u|     
            text << "<i>#{u+1}- #{User.find_by(id: i).name}</i>\n" 
          end
          send_message("<b>#{p.title}</b>#{p.content}\n#{p.dateplan}\n<b>Creado por:</b> #{p.user.name}\n<b>Asistentes:</b>\n"+text)
        else
          send_message("<b>#{p.title}</b>#{p.content}\n#{p.dateplan}\n<b>Creado por:</b> #{p.user.name}")
        end
      end
  end

  def save_plan(msg)
      plan_accesible  = User.find_by(telegram_id: msg.from.id, wait_plan: true)
      plan            = Plan.new
      message         = msg.text.each_line.to_a
      
      if message.size() == 3
        clean_message1   = message[0].split(" - ", 2)
        clean_message2   = message[1].split(" - ", 2)
        clean_message3   = message[2].split(" - ", 2)
      end

      if message.size() == 3 && clean_message1[0] == "title"   && plan_accesible
        plan.title    = clean_message1[1]
      end
      if message.size() == 3 && clean_message2[0] == "content" && plan_accesible
        plan.content  = clean_message2[1]
      end
      if message.size() == 3 && clean_message3[0] == "date"    && plan_accesible
        plan.dateplan = clean_message3[1]
      end
      if plan.title && plan.content && plan.dateplan
        plan.user_id  = User.find_by(telegram_id: msg.from.id).id
        plan.chat_id  = msg.chat.id
        plan.save
        plan_accesible.update(wait_plan: false)
        send_message("Plan añadido")
      end
    end

    def join_plan(user, title_plan)
      plan = Plan.find_by(title: title_plan)
      if plan.users_join.include?(user.id)
        send_message("Ya te has unido a este plan")
      else
        plan.users_join << user.id if !plan.users_join.include?(user.id)
        plan.save
        send_message("Te has unido al plan")
      end
    end

    def list_users_join(users_name)
      send_message("Usuario de este plan: #{users_name}")
    end

    def include_plan?
      message.text.include?('/plan ') && User.find_by(name: message.text.split(" ", 2)[1])
    end
    def include_the_following_text?(string)
      message.text.include?(string)
    end

    def user_telegram_id
      User.find_or_create_by(telegram_id: message.from.id, name: message.from.first_name)
    end

    def send_message(text)
      kb = Telegram::Bot::Types::ReplyKeyboardHide.new(hide_keyboard: true)
      bot.api.sendMessage(chat_id: message.chat.id, text: text, parse_mode: 'HTML', reply_markup: kb)
    end

    def search_plan_by_title
      if message.text.split(" ",2).size() >= 2
        Plan.find_by(title: message.text.split(" ", 2)[1]+"\n")
      end
    end


end