require "rubygems"
require "bundler/setup"
require "active_record"
require 'telegram/bot'
require 'pry'
require 'pry-byebug'
require './lib/connector-api'
require './lib/message_respond'
require './lib/reply_markup_formatter'
require './lib/emojize'
require 'erb'

# Load all of our ActiveRecord::Base objects.
class DatabaseConector
  def establish_connection
    
    project_root = File.dirname(File.absolute_path(__FILE__))
    Dir.glob(project_root + "/app/models/*.rb").each{|f| require f}

    template = ERB.new File.new("config/database.yml.erb").read
    connection_details = YAML.load template.result(binding)
    ActiveRecord::Base.establish_connection(connection_details)
  end
end
co = ConnectorApi.new(ENV['TELEGRAMTOKEN']).connection_api
